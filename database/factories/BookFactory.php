<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence($nbWords = 3, $variableNbWords = true), // generates fake sentence of 3 words
            'description' => $this->faker->realText($maxNbChars = 150, $indexSize = 2), // generates max 150 characters real text
            'publisher_id' => rand(1,4),    // gets random publisher_id between 1 to 4 as I'm generating 4 publishers
            'published_on' => $this->faker->dateTimeBetween('-20 days', now()), // generates date between todays date and pervious 20 days
        ];
    }
}
