<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');    // title of book
            $table->text('description');    // description of book
            $table->unsignedBigInteger('publisher_id'); // publisher_id
            $table->timestamp('published_on');  // date published on
            $table->timestamps();

            $table->foreign('publisher_id')
                ->references('id')
                ->on('publishers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
