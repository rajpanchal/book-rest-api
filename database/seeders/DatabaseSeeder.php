<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\Publisher;
use Illuminate\Database\Seeder;
use PhpParser\Node\Expr\FuncCall;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Publisher::factory()->count(4)->create();   // generates 4 publisher using factory of publisher
        Author::factory()->count(6)->create();  // generates 6 author using factory of author

        // generates 20 books using factory of book
        Book::factory()
            ->count(20)
            ->create()
            ->each(function(Book $book){
                $author = Author::all()->random(mt_rand(1,6))->pluck('id'); // get random 1 to 6 author_id
                $book->authors()->attach($author);  // attach book and author
            });
    }
}
