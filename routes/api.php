<?php

use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\PublishersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Resourceful route for books
Route::resource('books', BooksController::class)->only('index', 'show');

// Resourceful route for authors
Route::resource('authors', AuthorsController::class)->only('index', 'show');

// Resourceful route for publishers
Route::resource('publishers', PublishersController::class)->only('index', 'show');
