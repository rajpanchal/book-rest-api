<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Book extends Model
{
    use HasFactory;
    protected $guarded = [];

    /**
     * Book can belongs to many authors
     *
     */
    public function authors(): BelongsToMany {
        return $this->belongsToMany(Author::class);
    }

    /**
     * Book can belongs to only one publisher
     *
     */
    public function publisher(): BelongsTo {
        return $this->belongsTo(Publisher::class);
    }

}
