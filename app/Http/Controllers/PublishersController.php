<?php

namespace App\Http\Controllers;

use App\Models\Publisher;

class PublishersController extends Controller
{
    /**
     * Display a lists of the publishers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publishers = Publisher::all(); // Get list of publishers

        // return list of publishers
        return response()->json(['data' => $publishers, 'code' => 200], 200);
    }

    /**
     * Display the specified publisher.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function show(Publisher $publisher)
    {
        // return the specified publisher
        return response()->json(['data' => $publisher, 'code' => 200], 200);
    }
}
