<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Publisher;

class BooksController extends Controller
{
    /**
     * Display a list of the books.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();   // Get list of books

        // return list of books
        return response()->json(['data' => $books, 'code' => 200], 200);
    }

    /**
     * Display the specified book.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $authors = $book->authors;  // Get authors related to the specified book
        $publisher = Publisher::find($book->publisher_id);  // Get the publisher of the specified book

        // return book, authors and publisher
        return response()->json(['data' => [$book, $authors, $publisher], 'code' => 200], 200);
    }
}
