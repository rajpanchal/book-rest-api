# Book Rest API

## Contents
- [About The Project](#about-the-project)
    - [Built With](#built-with)
- [Dev Environment Setup](#dev-environment-setup)
    - [Prerequisties](#prerequisties)
    - [Installation](#installation)
- [ER Diagram](#er-diagram)
- [Contact](#contact)
- [License](#license)

<br/>

## About The Project

Aim of this assignment is to create a demo REST API to get information about books. The information retrieved from the REST API call is displayed using a standalone HTML page.

### Built With

* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Laravel](https://laravel.com)

<br/>

## Dev Environment Setup

To get a local copy up and running follow these simple example steps.

### Prerequisites

List of prerequisites you need to use the software and how to install them.

- [Download](https://getcomposer.org/download/) & install composer

```bash
composer install
```
- [Download](https://nodejs.org/en/download/) & install npm

- [Download](https://www.apachefriends.org/download.html) & install xampp v8.0
    - Start `Apache` & `MySQL` modules


## Installation

1. Copy the path from `path_to_xampp/php` folder and paste it to `Path` in system environment.

2. Clone the repo
   ```bash
   git clone https://gitlab.com/rajpanchal/book-rest-api.git
   ```

3. Cd into project folder and initialize git
   ```bash
   git cd book-rest-api
   git init
   ```

4. Create `.env` file & generate `APP_KEY`
   ```bash
   php artisan key:generate
   ```
   Create `booksrestapi` named database in your [phpmyadmin](http://localhost/phpmyadmin)

   Set below fields in .env file:
   
   `DB_DATABASE` = `booksrestapi` 
   
   `DB_USERNAME` = your username
   
   `DB_PASSWORD` = your password

5. Install NPM packages
   ```bash
   npm install && npm run dev
   ```

6. Open terminal in `book-rest-api` folder

7. Run migrations and seeder
   ```bash
   php artisan migrate:fresh --seed
   ```

8. Then run
   ```bash
   php artisan serve
   ```

9. Download the
   [Standalone HTML Page](https://drive.google.com/file/d/1sUw-N0Yn6m8RZCFq8q4T2zmcKZpIwE61/view?usp=sharing)

10. Double click and open index.html


## ER Diagram

![er-diagram](er-diagram-books.png)

## Contact
Email: rajpanchal.3786@gmail.com

## License
[MIT](https://choosealicense.com/licenses/mit/)
